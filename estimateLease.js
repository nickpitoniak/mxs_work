function estimateLeaseRate(leaseLenMonths, vehicleStickerPrice, downPayment, residualRate=0.55, salesTax=0.0725) {
  //format checks of incoming data likely to be malformed
  if(!(/^\+?\d+$/.test(String(leaseLenMonths))) || !(/^\+?\d+$/.test(String(vehicleStickerPrice))) || !(/^\+?\d+$/.test(String(downPayment)))) {
    return "Call For Rate";
  }
  //calculate raw lease rate estimation
  var leaseRateEstimation = Math.round(((vehicleStickerPrice * (salesTax + 1) - downPayment) - (vehicleStickerPrice * residualRate)) / leaseLenMonths);
  //format return to match US payment protocol format
  return "$" + String( leaseRateEstimation ) + ".00";
}

//example usage
var leaseLen = 36;
var stickerPrice = 17985;
var moneyDown = 2199;

console.log(estimateLeaseRate(leaseLen, stickerPrice, moneyDown));
