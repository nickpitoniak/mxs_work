import re
import os
import time
from random import randint

def noHupSystemCall(systemCall) :
  # fetch photos
  myCmd = 'nohup ' + systemCall
  os.system(myCmd)

def systemCall(systemCall) :
  # interact with kernel
  os.system(systemCall)

def get_url_images_in_text(text):
  # regex all image links
  return re.findall(r'(?:http\:|https\:)?\/\/.*\.(?:png|jpg)', text)

def writeToFile(inputData, filePath, extension) :
  with open(filePath + "." + extension, "w") as file:
    file.write(inputData)

def captureEnquotedText(text) :
  import re
  matches=re.findall(r'\"(.+?)\"',text)
  # matches is now ['String 1', 'String 2', 'String3']
  return matches

def convertRelativeLinksToStatic(inputText) :
  domain = "https://www.ford.com"
  txtret = inputText
  m = captureEnquotedText(inputText)
  for i in range(0, len(m) - 1) :
    if m[i][-4:] == ".jpg" or m[i][-5:] == ".jpeg" or m[i][-4:] == ".png" :
      if str(m[i][:1]) == "/" :
        txtret = textret.replace(str('"' + m[i]), '"' + domain + m[i])
      if str(m[i][:2]) == "./" :
        txtret = textret.replace(str('"' + m[i]), '"' + domain + "/cars" + m[i])
  return txtret

def extractImgs(inputFilePath) :
  txt = ""
  with open(inputFilePath) as file :
    # compile file data into a single string
    for line in file:
      txt = txt + line
    urls = get_url_images_in_text(txt)
    txt = convertRelativeLinksToStatic(txt)
    # build file for download
    timestamp = str(int(time.time()))
    systemCall("mkdir " + timestamp)
    systemCall("mkdir " + timestamp + "/imgs")
    # loop over image links in the file
    for i in range(0, len(urls)) :
      extension = urls[i].split(".")[len(urls[i].split(".")) - 1]
      randName = str(randint(10000, 100000)) + "." + extension
      syscallstr = "wget -O ./" + timestamp + "/imgs/" + randName + " " + str(urls[i])
      print("cloning     ./" + timestamp + "/imgs/" + randName + " " + str(urls[i]))
      noHupSystemCall(syscallstr)
      txt = txt.replace(urls[i], "./imgs/" + randName)
      writeToFile(txt, "./" + timestamp + "/index", "html")


extractImgs("./html/index.html")

